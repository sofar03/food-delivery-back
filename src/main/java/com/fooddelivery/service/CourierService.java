package com.fooddelivery.service;

import com.fooddelivery.model.Courier;

import java.util.List;

public interface CourierService {

    Courier save(Courier courier);
    Courier update(Courier courier);
    void delete(String id);

    Courier findById(String id);
    List<Courier> findAll();
    Courier findByUserId(String userId);

}
