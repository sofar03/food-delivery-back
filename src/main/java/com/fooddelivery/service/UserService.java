package com.fooddelivery.service;

import com.fooddelivery.model.User;

import java.util.List;

public interface UserService {

    User save(User user);
    User update(User user);
    void delete(String id);

    List<User> findAll();
    User findById(String id);
    User findByEmail(String email);

    User me();

}
