package com.fooddelivery.service;

import org.springframework.web.multipart.MultipartFile;

public interface AmazonS3Service {

    String uploadImage(MultipartFile image, String owner, String id);

}
