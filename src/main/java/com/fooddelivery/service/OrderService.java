package com.fooddelivery.service;

import com.fooddelivery.model.Order;
import com.fooddelivery.model.OrderStatus;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderService {

    Order save(Order order);
    Order update(Order order);
    void delete(String id);

    Order updateStatus(String orderId, OrderStatus orderStatus);

    Order findById(String id);
    Order findCurrent(String userId) throws Exception;
    List<Order> findAll();
    List<Order> findByUserId(String userId);
    List<Order> findAllBetweenCreationDate(LocalDateTime start, LocalDateTime end);

    Order addProduct(String orderId, String productId);
    Order deleteProduct(String orderId, String productId);

}
