package com.fooddelivery.service;

import com.fooddelivery.model.Supplier;
import com.fooddelivery.model.SupplierTag;

import java.util.List;

public interface SupplierService {

    Supplier save(Supplier supplier);
    Supplier update(Supplier supplier);
    void delete(String id);

    Supplier findById(String id);
    List<Supplier> findAll();
    List<Supplier> findAllCompact();
    List<Supplier> findByTags(SupplierTag tag);
    List<Supplier> findAllNames();
    List<Supplier> findTopSupplier(int limit);

    Supplier addProduct(String supplierId, String productId);
    void deleteProduct(String supplierId, String productId);

    Supplier addImage(String supplierId, String imageUrl);

}
