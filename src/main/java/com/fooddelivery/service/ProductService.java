package com.fooddelivery.service;

import com.fooddelivery.model.Product;
import com.fooddelivery.model.ProductType;

import java.util.List;
import java.util.Map;

public interface ProductService {

    Product save(Product product);
    Product update(Product product);
    void delete(String id);

    Product addImage(String productId, String imageUrl);

    List<Product> findAll();
    Product findById(String productId);
    List<Product> findBySupplierId(String supplierId);
    List<Product> findBySupplierIdAndProductType(String supplierId, ProductType productType);
    Map<ProductType, List<Product>> findBySupplierIdAndGroupedByType(String orderId);
    List<String> findAllNames();

}
