package com.fooddelivery.service;

import com.fooddelivery.dto.SearchTipDto;

import java.util.List;

public interface SearchService {

    List<SearchTipDto> findAllNamesForSearchableObjects();

}
