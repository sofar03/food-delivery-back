package com.fooddelivery.service;

import com.fooddelivery.dto.AuthDto;

public interface AuthenticationService {

    String authenticate(AuthDto authDto);

}
