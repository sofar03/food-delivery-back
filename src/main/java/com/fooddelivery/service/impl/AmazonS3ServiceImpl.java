package com.fooddelivery.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fooddelivery.service.AmazonS3Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Random;

@Service
public class AmazonS3ServiceImpl implements AmazonS3Service {

    private AmazonS3 s3client;

    @Value("${amazon.properties.url}")
    private String url;

    @Value("${amazon.properties.bucket}")
    private String bucket;

    @Value("${amazon.properties.region}")
    private String region;

    @Value("${amazon.properties.accessKey}")
    private String accessKey;

    @Value("${amazon.properties.secretKey}")
    private String secretKey;

    @Override
    public String uploadImage(MultipartFile image, String owner, String id) {
        PutObjectRequest request = null;

        String imageName = assembleImageName(owner, id, image.getContentType());
        try {

            request = new PutObjectRequest(bucket,
                    imageName,
                    image.getInputStream(),
                    new ObjectMetadata());
        } catch (IOException e) {
            e.printStackTrace();
        }

        s3client.putObject(request);
        return url + "/" + imageName;
    }

    @PostConstruct
    private void init() {
        AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
        this.s3client = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    private String assembleImageName(String owner, String id, String mime) {
        String extension = getExtension(mime);
        return owner + "_" + id + "." + extension;
    }

    private String getExtension(String mime) {
        return mime.substring(mime.lastIndexOf('/') + 1);
    }

}
