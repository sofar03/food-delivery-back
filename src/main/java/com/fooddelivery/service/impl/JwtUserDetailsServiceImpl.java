package com.fooddelivery.service.impl;

import com.fooddelivery.exception.EntityNotFoundException;
import com.fooddelivery.model.User;
import com.fooddelivery.security.UserPrincipal;
import com.fooddelivery.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    private final ModelMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user;
        try {
            user = userService.findByEmail(email);
        } catch (EntityNotFoundException ex) {
            throw new UsernameNotFoundException("User with email: " + email + " doesn't exists");
        }
        return mapper.map(user, UserPrincipal.class);
    }

}
