package com.fooddelivery.service.impl;

import com.fooddelivery.model.Product;
import com.fooddelivery.model.ProductType;
import com.fooddelivery.repository.OrderRepository;
import com.fooddelivery.repository.ProductRepository;
import com.fooddelivery.service.OrderService;
import com.fooddelivery.service.ProductService;
import com.fooddelivery.service.SupplierService;
import com.fooddelivery.utils.ServiceUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private SupplierService supplierService;

    @Override
    public Product save(Product product) {
        return productRepository.insert(product);
    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(String id) {
        Product product = findById(id);
        productRepository.delete(product);
    }

    @Override
    public Product addImage(String productId, String imageUrl) {
        Product product = findById(productId);
        product.setImageUrl(imageUrl);
        update(product);
        return product;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(String productId) {
        return ServiceUtils.getEntityIfExists(() -> productRepository.findById(productId),
                "Product was not found with id = " + productId);
    }

    @Override
    public List<Product> findBySupplierId(String supplierId) {
        supplierService.findById(supplierId); // throws an exception if supplier doesn't exists
        return productRepository.findBySupplierId(supplierId);
    }

    @Override
    public List<Product> findBySupplierIdAndProductType(String supplierId, ProductType productType) {
        supplierService.findById(supplierId); // throws an exception if supplier doesn't exists
        return productRepository.findBySupplierIdAndProductType(supplierId, productType);
    }

    @Override
    public Map<ProductType, List<Product>> findBySupplierIdAndGroupedByType(String supplierId) {
        supplierService.findById(supplierId);

        List<Product> products = productRepository.findBySupplierId(supplierId);

        System.out.println(products);
        System.out.println(products.stream()
                .collect(
                        Collectors.groupingBy(Product::getProductType,
                                Collectors.toList())));

        return products.stream()
                .collect(
                        Collectors.groupingBy(Product::getProductType,
                                Collectors.toList()));
    }

    @Override
    public List<String> findAllNames() {
        return productRepository.findAllNames();
    }

    @Autowired
    public void setSupplierService(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

}
