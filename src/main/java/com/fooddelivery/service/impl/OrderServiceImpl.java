package com.fooddelivery.service.impl;

import com.fooddelivery.model.*;
import com.fooddelivery.repository.OrderRepository;
import com.fooddelivery.service.OrderService;
import com.fooddelivery.service.ProductService;
import com.fooddelivery.service.UserService;
import com.fooddelivery.utils.ServiceUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private ProductService productService;
    private final UserService userService;

    @Override
    public Order save(Order order) {
        User user = userService.findById(order.getUser().getId());
        user.getOrders().add(order);
        userService.update(user);

        // Checking if product exists
        if (order.getProducts() != null) {
            for (ProductEntry product : order.getProducts()) {
                productService.findById(product.getProduct().getId());
            }
        }

        order.setId(null);
        order.setCreationDate(LocalDateTime.now());
        order.setStatus(OrderStatus.IN_PROGRESS);

        return orderRepository.insert(order);
    }

    @Override
    public Order update(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void delete(String id) {
        Order order = findById(id);

        User user = userService.findById(order.getUser().getId());
        user.getOrders().remove(order);
        userService.update(user);

        orderRepository.delete(findById(id));
    }

    @Override
    public Order updateStatus(String orderId, OrderStatus orderStatus) {
        Order order = findById(orderId);
        order.setStatus(orderStatus);
        update(order);
        return order;
    }

    @Override
    public Order findById(String id) {
        return ServiceUtils
                .getEntityIfExists(() -> orderRepository.findById(id),
                        "Order was not found with id = " + id);
    }

    @Override
    public Order findCurrent(String userId) throws Exception {
        Sort sort = Sort.by(Sort.Direction.DESC, "creationDate");
        List<Order> orders = orderRepository.findByUserId(userId, sort);

        if (orders.size() < 1) {
            throw new Exception("User with id = " + userId + " doesn't have orders");
        }

        Order order = orders.get(0);

        if (order.getStatus().equals(OrderStatus.IN_PROGRESS)) {
            return order;
        }
        throw new Exception("There is no active orders in user with id = " + userId);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> findByUserId(String userId) {
        return orderRepository.findByUserId(userId);
    }

    @Override
    public List<Order> findAllBetweenCreationDate(LocalDateTime start, LocalDateTime end) {
        return orderRepository.findByCreationDateBetween(start, end);
    }

    @Override
    public Order addProduct(String orderId, String productId) {
        Order order = findById(orderId);
        Product product = productService.findById(productId);

        if (order.getProducts().stream().anyMatch(p -> p.getProduct().equals(product))) {
            ProductEntry productEntry = order.getProducts().stream()
                    .filter(p -> p.getProduct().equals(product))
                    .findFirst()
                    .get();
            productEntry.setAmount(productEntry.getAmount() + 1);
        } else {
            order.getProducts().add(ProductEntry.builder().product(product).amount(1).build());
        }

        order.setTotal(order.getTotal() + product.getPrice());

        orderRepository.save(order);
        return order;
    }

    @Override
    public Order deleteProduct(String orderId, String productId) {
        Order order = findById(orderId);
        Product product = productService.findById(productId);

        ProductEntry productEntry = order.getProducts().stream()
                .filter(p -> p.getProduct().equals(product))
                .findFirst()
                .get();
        productEntry.setAmount(productEntry.getAmount() - 1);
        if (productEntry.getAmount() == 0) {
            order.getProducts().removeIf(p -> p.getProduct().getId().equals(product.getId()));
        }

        order.setTotal(order.getTotal() - product.getPrice());

        orderRepository.save(order);
        return order;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

}
