package com.fooddelivery.service.impl;

import com.fooddelivery.dto.SearchTipDto;
import com.fooddelivery.service.ProductService;
import com.fooddelivery.service.SearchService;
import com.fooddelivery.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final ProductService productService;
    private final SupplierService supplierService;

    @Override
    public List<SearchTipDto> findAllNamesForSearchableObjects() {
        List<SearchTipDto> supplierAllNames = supplierService
                .findAllNames()
                .stream()
                .map(s -> SearchTipDto.builder()
                        .id(s.getId())
                        .tip(s.getName())
                        .objectType("Supplier")
                        .build())
                .collect(Collectors.toList());

        /*List<SearchTipDto> productAllNames = productService
                .findAllNames()
                .stream()
                .map(s -> SearchTipDto.builder()
                        .tip(s)
                        .objectType("Product")
                        .build())
                .collect(Collectors.toList());*/

        List<SearchTipDto> allNameTips = new ArrayList<>();
        allNameTips.addAll(supplierAllNames);
        //allNameTips.addAll(productAllNames);
        return allNameTips;
    }

}
