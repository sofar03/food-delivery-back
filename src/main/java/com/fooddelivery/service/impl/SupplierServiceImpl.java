package com.fooddelivery.service.impl;

import com.fooddelivery.exception.EntityAlreadyAttachedException;
import com.fooddelivery.model.Order;
import com.fooddelivery.model.Product;
import com.fooddelivery.model.Supplier;
import com.fooddelivery.model.SupplierTag;
import com.fooddelivery.repository.SupplierRepository;
import com.fooddelivery.service.OrderService;
import com.fooddelivery.service.ProductService;
import com.fooddelivery.service.SupplierService;
import com.fooddelivery.utils.ServiceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SupplierServiceImpl implements SupplierService {

    private final SupplierRepository supplierRepository;

    private final OrderService orderService;
    private ProductService productService;

    @Override
    public Supplier save(Supplier supplier) {
        return supplierRepository.insert(supplier);
    }

    @Override
    public Supplier update(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public Supplier findById(String id) {
        return ServiceUtils
                .getEntityIfExists(() -> supplierRepository.findById(id),
                        "Supplier was not found with id = " + id);
    }

    @Override
    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }

    @Override
    public List<Supplier> findAllCompact() {
        return supplierRepository.findAllCompact();
    }

    @Override
    public List<Supplier> findByTags(SupplierTag tag) {
        return supplierRepository.findByTags(tag);
    }

    @Override
    public List<Supplier> findAllNames() {
        return supplierRepository.findAllNames();
    }

    @Override
    public List<Supplier> findTopSupplier(int limit) {
        List<Order> allOrders = orderService.findAll();

        return allOrders.stream()
                .map(Order::getProducts)
                .flatMap(Collection::stream)
                .collect(
                        Collectors.groupingBy(
                                p -> p.getProduct().getSupplier(),
                                Collectors.toList()))
                .entrySet()
                .stream()
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                e -> (long) e.getValue().size()
                        ))
                .entrySet()
                .stream()
                .sorted((e0, e1) -> (int) (e1.getValue() - e0.getValue()))
                .limit(limit)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public Supplier addProduct(String supplierId, String productId) {
        Supplier supplier = findById(supplierId);
        Product product = productService.findById(productId);

        if (product.getSupplier() != null) {
            throw new EntityAlreadyAttachedException("Product with id: " + productId + ", already has supplier");
        }

        supplier.getProducts().add(product);
        product.setSupplier(supplier);

        productService.update(product);
        supplierRepository.save(supplier);
        return supplier;
    }

    @Override
    public void deleteProduct(String supplierId, String productId) {
        Supplier supplier = findById(supplierId);
        supplier.getProducts().removeIf(p -> p.getId().equals(productId));
        supplierRepository.save(supplier);
    }

    @Override
    public Supplier addImage(String supplierId, String imageUrl) {
        Supplier supplier = findById(supplierId);
        supplier.setImageUrl(imageUrl);
        update(supplier);
        return supplier;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

}
