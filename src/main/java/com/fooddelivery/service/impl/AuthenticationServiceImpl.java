package com.fooddelivery.service.impl;

import com.fooddelivery.dto.AuthDto;
import com.fooddelivery.exception.EntityNotFoundException;
import com.fooddelivery.model.User;
import com.fooddelivery.security.UserPrincipal;
import com.fooddelivery.service.AuthenticationService;
import com.fooddelivery.service.JwtService;
import com.fooddelivery.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserService userService;
    private final JwtService jwtService;

    private final ModelMapper mapper;

    private final PasswordEncoder passwordEncoder;

    @Value("${jwt.token.authentication.expire-date}")
    private long jwtTokenAuthenticationExpireDate;

    /**
     * Authenticates user by username and password
     * @param authDto contains username and password
     * @return JWT Authentication Token
     */
    @Override
    public String authenticate(AuthDto authDto) {
        User user;

        try {
            user = userService.findByEmail(authDto.getEmail());
        } catch (EntityNotFoundException ex) {
            throw new BadCredentialsException("User not found");
        }

        if ( !verifyUser(authDto, user)) {
            throw new BadCredentialsException("Email or password incorrect");
        }

        log.info("User successfully authenticated");
        UserPrincipal principal = mapper.map(user, UserPrincipal.class);

        return jwtService.createToken(principal.getUser(), jwtTokenAuthenticationExpireDate);
    }

    private boolean verifyUser(AuthDto authDto, User user) {
        return user.getEmail().equals(authDto.getEmail())
                && passwordEncoder.matches(authDto.getPassword(), user.getPassword());
    }

}
