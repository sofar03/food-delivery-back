package com.fooddelivery.service.impl;

import com.fooddelivery.exception.EntityAlreadyExists;
import com.fooddelivery.exception.EntityNotFoundException;
import com.fooddelivery.model.User;
import com.fooddelivery.repository.UserRepository;
import com.fooddelivery.security.UserPrincipal;
import com.fooddelivery.service.UserService;
import com.fooddelivery.utils.ServiceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper mapper;

    private PasswordEncoder passwordEncoder;

    @Override
    public User save(User user) {
        try {
            findByEmail(user.getEmail());
        } catch (EntityNotFoundException ignore) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.insert(user);
        }
        throw new EntityAlreadyExists("User already exists");
    }

    @Override
    public User update(User user) {
        User oldUser = findById(user.getId());
        mapper.map(user, oldUser); // Updates the user and skips the NULL fields
        return userRepository.save(oldUser);
    }

    @Override
    public void delete(String id) {
        userRepository.delete(findById(id));
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        return ServiceUtils
                .getEntityIfExists(() -> userRepository.findById(id),
                        "User was not found with id = " + id);
    }

    @Override
    public User findByEmail(String email) {
        return ServiceUtils
                .getEntityIfExists(() -> userRepository.findByEmail(email),
                        "User was not found with email = " + email);
    }

    @Override
    public User me() {
        Object principal = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserPrincipal)principal).getUser();
        } else {
            throw new EntityNotFoundException("User is not authenticated");
        }
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
