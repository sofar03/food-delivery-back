package com.fooddelivery.service.impl;

import com.fooddelivery.model.Courier;
import com.fooddelivery.repository.CourierRepository;
import com.fooddelivery.service.CourierService;
import com.fooddelivery.service.UserService;
import com.fooddelivery.utils.ServiceUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CourierServiceImpl implements CourierService {

    private final CourierRepository courierRepository;

    private final UserService userService;

    @Override
    public Courier save(Courier courier) {

        return courierRepository.insert(courier);
    }

    @Override
    public Courier update(Courier courier) {
        return courierRepository.save(courier);
    }

    @Override
    public void delete(String id) {
        Courier courier = findById(id);
        courierRepository.delete(courier);
    }

    @Override
    public Courier findById(String id) {
        return ServiceUtils
                .getEntityIfExists(() -> courierRepository.findById(id),
                        "Courier was not found with id = " + id);
    }

    @Override
    public List<Courier> findAll() {
        return courierRepository.findAll();
    }

    @Override
    public Courier findByUserId(String userId) {
        userService.findById(userId); // Throws an exception if user do not exists
        return ServiceUtils
                .getEntityIfExists(() -> courierRepository.findByUserId(userId),
                        "Courier was not found with user id = " + userId);
    }

}
