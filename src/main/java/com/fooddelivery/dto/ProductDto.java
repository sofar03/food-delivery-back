package com.fooddelivery.dto;

import com.fooddelivery.model.Dimensions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private String name;
    private String productType;
    private double price;
    private Dimensions dimensions;

}
