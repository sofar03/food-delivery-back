package com.fooddelivery.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainOrderDto {

    private String id;
    private double total;
    private String status;
    private String creationDate;
    private List<MainProductEntryDto> products;

}
