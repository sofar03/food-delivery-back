package com.fooddelivery.dto;

import com.fooddelivery.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainUserDto {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String city;
    private String address;
    private Set<Role> roles;

}
