package com.fooddelivery.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainSupplierDto {

    private String id;
    private String name;
    private String address;
    private String imageUrl;
    private List<MainProductDto> products;
    private Set<String> tags;

}
