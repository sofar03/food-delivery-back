package com.fooddelivery.dto;

import com.fooddelivery.model.CourierStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainCourierDto {

    private MainUserDto user;
    private List<MainOrderDto> orders;
    private CourierStatus status;

}
