package com.fooddelivery.dto;

import com.fooddelivery.model.Dimensions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainProductDto {

    private String id;
    private String name;
    private String productType;
    private double price;
    private Dimensions dimensions;
    private String imageUrl;

}
