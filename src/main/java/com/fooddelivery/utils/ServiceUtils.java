package com.fooddelivery.utils;

import com.fooddelivery.exception.EntityNotFoundException;

import java.util.Optional;
import java.util.function.Supplier;

public class ServiceUtils {

    public static <T> T getEntityIfExists(Supplier<Optional<T>> supplier, String exceptionMessage) {
        Optional<T> t = supplier.get();
        if (t.isEmpty()) {
            throw new EntityNotFoundException(exceptionMessage);
        }
        return t.get();
    }

}
