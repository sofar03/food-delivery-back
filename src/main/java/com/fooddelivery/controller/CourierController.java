package com.fooddelivery.controller;

import com.fooddelivery.dto.CourierDto;
import com.fooddelivery.dto.MainCourierDto;
import com.fooddelivery.dto.MainUserDto;
import com.fooddelivery.model.Courier;
import com.fooddelivery.service.CourierService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/couriers")
@RequiredArgsConstructor
@Api(tags = "Couriers")
public class CourierController {

    private final CourierService courierService;

    private final ModelMapper mapper;

    @GetMapping
    public List<MainCourierDto> getAll() {
        return courierService.findAll().stream()
                .map(u -> mapper.map(u, MainCourierDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/byUserId/{userId}")
    public MainCourierDto getByUserId(@PathVariable String userId) {
        Courier courier = courierService.findByUserId(userId);
        return mapper.map(courier, MainCourierDto.class);
    }

    @PostMapping("/new")
    public MainCourierDto save(@RequestBody CourierDto courierDto) {
        Courier courier = mapper.map(courierDto, Courier.class);
        Courier newCourier = courierService.save(courier);
        return mapper.map(newCourier, MainCourierDto.class);
    }

}
