package com.fooddelivery.controller;

import com.fooddelivery.dto.AuthDto;
import com.fooddelivery.service.AuthenticationService;
import com.fooddelivery.utils.CookieUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@Api(tags = "Authentication")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @Value("${jwt.authentication.cookie.expire-date}")
    private int jwtCookieExpireDate;

    @Value("${jwt.token.cookie.name}")
    private String jwtTokenCookieName;

    @PostMapping(value = "/authenticate")
    public void authenticate(@RequestBody AuthDto authDto,
                               HttpServletResponse response) {
        String token = authenticationService.authenticate(authDto);
        CookieUtils.addCookie(response, jwtTokenCookieName, token, jwtCookieExpireDate);
    }

    @PostMapping(value = "/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        CookieUtils.deleteCookie(request, response, jwtTokenCookieName);
    }

}
