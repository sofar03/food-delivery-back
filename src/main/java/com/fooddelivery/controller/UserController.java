package com.fooddelivery.controller;

import com.fooddelivery.dto.MainUserDto;
import com.fooddelivery.dto.UserDto;
import com.fooddelivery.model.User;
import com.fooddelivery.service.UserService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
@Api(tags = "Users")
public class UserController {

    private final UserService userService;

    private final ModelMapper mapper;

    @GetMapping
    public List<MainUserDto> getAll() {
        return userService.findAll().stream()
                .map(u -> mapper.map(u, MainUserDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/me")
    public MainUserDto me() {
        User me = userService.me();
        return mapper.map(me, MainUserDto.class);
    }

    @GetMapping("/byId/{userId}")
    public MainUserDto getAll(@PathVariable String userId) {
        User user = userService.findById(userId);
        return mapper.map(user, MainUserDto.class);
    }

    @PostMapping("/new")
    public MainUserDto save(@RequestBody UserDto userDto) {
        User user = mapper.map(userDto, User.class);
        User newUser = userService.save(user);
        return mapper.map(newUser, MainUserDto.class);
    }

    @PutMapping("/{userId}/update")
    public MainUserDto update(@PathVariable String userId, @RequestBody UserDto userDto) {
        User user = mapper.map(userDto, User.class);
        user.setId(userId);

        User newUser = userService.update(user);
        return mapper.map(newUser, MainUserDto.class);
    }

}
