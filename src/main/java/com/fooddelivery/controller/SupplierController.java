package com.fooddelivery.controller;

import com.fooddelivery.dto.MainSupplierDto;
import com.fooddelivery.dto.SupplierDto;
import com.fooddelivery.model.Supplier;
import com.fooddelivery.model.SupplierTag;
import com.fooddelivery.service.AmazonS3Service;
import com.fooddelivery.service.SupplierService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/suppliers")
@RequiredArgsConstructor
@Api(tags = "Suppliers")
public class SupplierController {

    private final SupplierService supplierService;

    private final AmazonS3Service amazonS3Service;

    private final ModelMapper mapper;

    @GetMapping
    public List<MainSupplierDto> getAll() {
        return supplierService.findAll().stream()
                .map(s -> mapper.map(s, MainSupplierDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/compact")
    public List<MainSupplierDto> getAllCompact() {
        return supplierService.findAllCompact().stream()
                .map(s -> mapper.map(s, MainSupplierDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/byTag/{tag}")
    public List<MainSupplierDto> getByTag(@PathVariable String tag) {
        List<Supplier> suppliers = supplierService.findByTags(SupplierTag.valueOf(tag));
        return suppliers.stream()
                .map(s -> mapper.map(s, MainSupplierDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/byId/{supplierId}")
    public MainSupplierDto getById(@PathVariable String supplierId) {
        Supplier supplier = supplierService.findById(supplierId);
        return mapper.map(supplier, MainSupplierDto.class);
    }

    @GetMapping(value = "/topSuppliers", params = { "limit" })
    public List<MainSupplierDto> getTopSuppliers(@RequestParam(required = false, defaultValue = "10") int limit) {
        List<Supplier> topSupplier = supplierService.findTopSupplier(limit);
        return topSupplier.stream()
                .map(s -> mapper.map(s, MainSupplierDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/allTags")
    public List<String> getAllTags() {
        return Arrays.stream(SupplierTag.values())
                .map(v -> {
                    String lowerCase = v.name().substring(1).toLowerCase();
                    return (v.name().charAt(0) + lowerCase).replace('_', ' ');
                })
                .collect(Collectors.toList());
    }

    @PostMapping("/new")
    public MainSupplierDto save(@RequestBody SupplierDto supplierDto) {
        Supplier supplier = mapper.map(supplierDto, Supplier.class);
        Supplier newSupplier = supplierService.save(supplier);
        return mapper.map(newSupplier, MainSupplierDto.class);
    }

    @PostMapping("/{supplierId}/addImage")
    public MainSupplierDto addImage(@PathVariable String supplierId,
                                   @RequestBody MultipartFile image) {
        String imageUrl = amazonS3Service.uploadImage(image, "supplier", supplierId);
        Supplier supplier = supplierService.addImage(supplierId, imageUrl);
        return mapper.map(supplier, MainSupplierDto.class);
    }

    @PostMapping("/{supplierId}/add/product")
    public MainSupplierDto addProduct(@PathVariable String supplierId, @RequestBody String productId) {
        Supplier supplier = supplierService.addProduct(supplierId, productId);
        return mapper.map(supplier, MainSupplierDto.class);
    }

    @DeleteMapping("/{supplierId}/delete/product")
    public void deleteProduct(@PathVariable String supplierId, @RequestBody String productId) {
        supplierService.deleteProduct(supplierId, productId);
    }

}
