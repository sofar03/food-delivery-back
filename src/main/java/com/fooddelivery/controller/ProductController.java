package com.fooddelivery.controller;

import com.fooddelivery.dto.MainProductDto;
import com.fooddelivery.dto.ProductDto;
import com.fooddelivery.model.Product;
import com.fooddelivery.model.ProductType;
import com.fooddelivery.service.AmazonS3Service;
import com.fooddelivery.service.ProductService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/products")
@RequiredArgsConstructor
@Api(tags = "Products")
public class ProductController {

    private final ProductService productService;
    private final AmazonS3Service amazonS3Service;

    private final ModelMapper mapper;

    @GetMapping
    public List<MainProductDto> getAll() {
        return productService.findAll().stream()
                .map(p -> mapper.map(p, MainProductDto.class))
                .collect(Collectors.toList());
    }

    @PostMapping("/new")
    public MainProductDto save(@RequestBody ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        Product newProduct = productService.save(product);
        return mapper.map(newProduct, MainProductDto.class);
    }

    @PostMapping("/{productId}/addImage")
    public MainProductDto addImage(@PathVariable String productId,
                                   @RequestBody MultipartFile image) {
        String imageUrl = amazonS3Service.uploadImage(image, "product", productId);
        Product product = productService.addImage(productId, imageUrl);
        return mapper.map(product, MainProductDto.class);
    }

    @GetMapping("/bySupplierId/{supplierId}")
    public List<MainProductDto> getAllProductsBySupplierId(@PathVariable String supplierId) {
        List<Product> products = productService.findBySupplierId(supplierId);
        return products.stream()
                .map(p -> mapper.map(p, MainProductDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/bySupplierIdType/{supplierId}/{productType}")
    public List<MainProductDto> getProductsByTypeAndSupplierId(@PathVariable String supplierId,
                                                               @PathVariable String productType) {
        List<Product> products = productService
                .findBySupplierIdAndProductType(supplierId, ProductType.valueOf(productType));
        return products.stream()
                .map(p -> mapper.map(p, MainProductDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/{supplierId}/grouped")
    public Map<String, List<MainProductDto>> getCollectedProductsByTypeAndSupplierId(@PathVariable String supplierId) {
        return productService
                .findBySupplierIdAndGroupedByType(supplierId)
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        e -> e.getKey().name(),
                        e -> e.getValue().stream()
                                .map(p -> mapper.map(p, MainProductDto.class))
                                .collect(Collectors.toList())
                ));
    }

}
