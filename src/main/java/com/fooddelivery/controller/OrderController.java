package com.fooddelivery.controller;

import com.fooddelivery.dto.MainOrderDto;
import com.fooddelivery.dto.OrderDto;
import com.fooddelivery.model.Order;
import com.fooddelivery.model.OrderStatus;
import com.fooddelivery.service.OrderService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/orders")
@RequiredArgsConstructor
@Api(tags = "Orders")
public class OrderController {

    private final OrderService orderService;

    private final ModelMapper mapper;

    @GetMapping
    public List<MainOrderDto> getAll() {
        return orderService.findAll().stream()
                .map(o -> mapper.map(o, MainOrderDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/getCurrent/{userId}")
    public MainOrderDto getCurrent(@PathVariable String userId) throws Exception {
        Order order = orderService.findCurrent(userId);
        return mapper.map(order, MainOrderDto.class);
    }

    @GetMapping("/byUserId/{userId}")
    public List<MainOrderDto> getByUserId(@PathVariable String userId) {
        return orderService.findByUserId(userId).stream()
                .sorted(Comparator.comparing(Order::getCreationDate, Comparator.reverseOrder()))
                .map(o -> mapper.map(o, MainOrderDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/byId/{orderId}")
    public MainOrderDto getById(@PathVariable String orderId) {
        return mapper.map(orderService.findById(orderId), MainOrderDto.class);
    }

    @PostMapping("/new")
    public MainOrderDto create(@RequestBody OrderDto orderDto) {
        Order order = mapper.map(orderDto, Order.class);
        return mapper.map(orderService.save(order), MainOrderDto.class);
    }

    @PostMapping("{orderId}/add/product")
    public MainOrderDto addProduct(@PathVariable String orderId, @RequestBody String productId) {
        Order order = orderService.addProduct(orderId, productId);
        return mapper.map(order, MainOrderDto.class);
    }

    @PostMapping("/{orderId}/update/status")
    public MainOrderDto updateStatus(@PathVariable String orderId, @RequestBody String orderStatus) {
        Order order = orderService.updateStatus(orderId, OrderStatus.valueOf(orderStatus));
        return mapper.map(order, MainOrderDto.class);
    }

    @DeleteMapping("{orderId}/delete/product/{productId}")
    public MainOrderDto deleteProduct(@PathVariable String orderId, @PathVariable String productId) {
        Order order = orderService.deleteProduct(orderId, productId);
        return mapper.map(order, MainOrderDto.class);
    }

    @DeleteMapping("/{orderId}")
    public void delete(@PathVariable String orderId) {
        orderService.delete(orderId);
    }

}
