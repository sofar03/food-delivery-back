package com.fooddelivery.controller;

import com.fooddelivery.dto.SearchTipDto;
import com.fooddelivery.service.SearchService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/search")
@RequiredArgsConstructor
@Api(tags = "Search")
public class SearchController {

    private final SearchService searchService;

    @GetMapping("allTips")
    public List<SearchTipDto> getAllTips() {
        return searchService.findAllNamesForSearchableObjects();
    }

}
