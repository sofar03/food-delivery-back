package com.fooddelivery.repository;

import com.fooddelivery.model.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {

    List<Order> findByUserId(String userId);
    List<Order> findByUserId(String userId, Sort sort);
    List<Order> findByCreationDateBetween(LocalDateTime start, LocalDateTime end);

}
