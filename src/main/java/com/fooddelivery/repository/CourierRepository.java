package com.fooddelivery.repository;

import com.fooddelivery.model.Courier;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourierRepository extends MongoRepository<Courier, String> {

    Optional<Courier> findByUserId(String userId);

}
