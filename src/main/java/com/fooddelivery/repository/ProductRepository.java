package com.fooddelivery.repository;

import com.fooddelivery.model.Product;
import com.fooddelivery.model.ProductType;
import com.fooddelivery.model.Supplier;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {

    List<Product> findByProductType(ProductType productType);
    List<Product> findBySupplierId(String supplierId);
    List<Product> findBySupplierIdAndProductType(String supplierId, ProductType productType);

    default List<String> findAllNames() {
        return findAll().stream().map(Product::getName).collect(Collectors.toList());
    }

}
