package com.fooddelivery.repository;

import com.fooddelivery.model.Supplier;
import com.fooddelivery.model.SupplierTag;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public interface SupplierRepository extends MongoRepository<Supplier, String> {

    List<Supplier> findByTags(SupplierTag tag);

    @Query(fields = "{ 'name': 1, 'imageUrl': 1 }", value = "{'id': {$exists: true}}")
    List<Supplier> findAllCompact();

    default List<Supplier> findAllNames() {
        return findAll()
                .stream()
                .map(a ->
                        Supplier.builder()
                                .id(a.getId())
                                .name(a.getName())
                                .build())
                .collect(Collectors.toList());
    }

}
