package com.fooddelivery.converter;

import com.fooddelivery.model.SupplierTag;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class SupplierTypeConverter implements Converter<String, SupplierTag> {

    @Override
    public SupplierTag convert(MappingContext<String, SupplierTag> context) {
        return SupplierTag.valueOf(context.getSource());
    }

}
