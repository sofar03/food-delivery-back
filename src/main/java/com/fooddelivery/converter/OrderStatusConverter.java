package com.fooddelivery.converter;

import com.fooddelivery.model.OrderStatus;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusConverter implements Converter<String, OrderStatus> {

    @Override
    public OrderStatus convert(MappingContext<String, OrderStatus> context) {
        return OrderStatus.valueOf(context.getSource());
    }

}
