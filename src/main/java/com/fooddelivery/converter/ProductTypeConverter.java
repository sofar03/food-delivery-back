package com.fooddelivery.converter;

import com.fooddelivery.model.ProductType;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class ProductTypeConverter implements Converter<String, ProductType> {

    @Override
    public ProductType convert(MappingContext<String, ProductType> context) {
        return ProductType.valueOf(context.getSource());
    }

}
