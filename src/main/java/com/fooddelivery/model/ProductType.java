package com.fooddelivery.model;

public enum ProductType {

    PIZZA,
    BURGER,
    SOUP,
    ROLL,
    STEAK,
    BEVERAGE,
    DESERT,
    CROISSANT,
    BREAD,
    PASTA,
    SALAD

}
