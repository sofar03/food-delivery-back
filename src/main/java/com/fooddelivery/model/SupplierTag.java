package com.fooddelivery.model;

public enum SupplierTag {

    AMERICAN,
    ITALIAN,
    FAST_FOOD,
    GEORGIAN,
    DESSERT,
    BAKERY,
    ASIAN,
    ALL

}
