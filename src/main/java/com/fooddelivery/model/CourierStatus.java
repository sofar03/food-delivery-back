package com.fooddelivery.model;

public enum CourierStatus {

    DELIVERS,
    WAITING_FOR_ORDER

}
