package com.fooddelivery.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document(collection = "couriers")
public class Courier extends User {

    @DBRef
    private User user;

    @DBRef
    @Field("courierOrders")
    private List<Order> orders = new ArrayList<>();

    private CourierStatus status;

    @Builder(builderMethodName = "courierBuilder")
    public Courier(String id, User user, List<Order> orders, CourierStatus status) {
        this.user = user;
        this.orders = orders;
        this.status = status;
    }

}
