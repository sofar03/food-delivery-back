package com.fooddelivery.model;

public enum OrderStatus {

    IN_THE_WAY,
    IN_PROGRESS,
    CANCELED,
    DONE

}
