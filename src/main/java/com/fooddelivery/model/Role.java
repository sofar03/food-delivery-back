package com.fooddelivery.model;

public enum Role {

    USER,
    COURIER,
    MANAGER,
    ADMIN

}
