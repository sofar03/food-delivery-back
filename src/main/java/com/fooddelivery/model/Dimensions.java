package com.fooddelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Dimensions {

    // In centimeters
    private double width;

    // In centimeters
    private double height;

    // In centimeters
    private double length;

    // In grams
    private double weight;

}
