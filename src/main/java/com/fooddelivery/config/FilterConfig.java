package com.fooddelivery.config;

import com.fooddelivery.security.AccessControlHeaderFilter;
import com.fooddelivery.security.JwtAuthenticationLoginRetryFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FilterConfig {

    private final JwtAuthenticationLoginRetryFilter jwtAuthenticationLoginRetryFilter;

    @Bean
    public FilterRegistrationBean<JwtAuthenticationLoginRetryFilter> deleteLoginJwtAuthenticationTokenFilter() {
        FilterRegistrationBean<JwtAuthenticationLoginRetryFilter> registrationBean =
                new FilterRegistrationBean<>();
        registrationBean.setOrder(-101);
        registrationBean.setFilter(jwtAuthenticationLoginRetryFilter);
        registrationBean.addUrlPatterns(
                "/authenticate");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<AccessControlHeaderFilter> corsFilter() {
        FilterRegistrationBean<AccessControlHeaderFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.setOrder(-102);
        registrationBean.setFilter(new AccessControlHeaderFilter());
        registrationBean.addUrlPatterns("*");

        return registrationBean;
    }

}
