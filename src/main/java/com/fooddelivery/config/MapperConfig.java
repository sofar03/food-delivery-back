package com.fooddelivery.config;

import com.fooddelivery.converter.OrderStatusConverter;
import com.fooddelivery.converter.ProductTypeConverter;
import com.fooddelivery.converter.SupplierTypeConverter;
import com.fooddelivery.dto.CourierDto;
import com.fooddelivery.dto.OrderDto;
import com.fooddelivery.dto.ProductEntryDto;
import com.fooddelivery.mapper.CourierMapper;
import com.fooddelivery.mapper.OrderMapper;
import com.fooddelivery.mapper.ProductEntryMapper;
import com.fooddelivery.mapper.UserPrincipalMapper;
import com.fooddelivery.model.*;
import com.fooddelivery.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class MapperConfig {

    private final ProductEntryMapper productEntryMapper;
    private final OrderMapper orderMapper;
    private final CourierMapper courierMapper;
    private final UserPrincipalMapper userPrincipalMapper;

    private final ProductTypeConverter productTypeConverter;
    private final SupplierTypeConverter supplierTypeConverter;
    private final OrderStatusConverter orderStatusConverter;

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();

        mapper.createTypeMap(ProductEntryDto.class, ProductEntry.class)
                .addMappings(productEntryMapper.toModel);

        mapper.createTypeMap(ProductEntry.class, ProductEntryDto.class)
                .addMappings(productEntryMapper.toDto);

        mapper.createTypeMap(CourierDto.class, Courier.class)
                .addMappings(courierMapper.toModel);

        mapper.createTypeMap(OrderDto.class, Order.class)
                .addMappings(orderMapper.toModel);

        mapper.createTypeMap(Order.class, OrderDto.class)
                .addMappings(orderMapper.toDto);

        mapper.createTypeMap(User.class, UserPrincipal.class)
                .addMappings(userPrincipalMapper.toPrincipal);

        mapper.addConverter(productTypeConverter);
        mapper.addConverter(supplierTypeConverter);
        mapper.addConverter(orderStatusConverter);

        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());

        return mapper;
    }

}
