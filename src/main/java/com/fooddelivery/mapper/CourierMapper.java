package com.fooddelivery.mapper;

import com.fooddelivery.dto.CourierDto;
import com.fooddelivery.model.Courier;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class CourierMapper {

    public PropertyMap<CourierDto, Courier> toModel = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source.getUserId(), destination.getUser().getId());
        }
    };

}
