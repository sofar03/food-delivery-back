package com.fooddelivery.mapper;

import com.fooddelivery.model.User;
import com.fooddelivery.security.UserPrincipal;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class UserPrincipalMapper {

    public PropertyMap<User, UserPrincipal> toPrincipal = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source, destination.getUser());
        }
    };

}
