package com.fooddelivery.mapper;

import com.fooddelivery.dto.ProductEntryDto;
import com.fooddelivery.model.ProductEntry;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductEntryMapper {

    public PropertyMap<ProductEntryDto, ProductEntry> toModel = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source.getProductId(), destination.getProduct().getId());
        }
    };

    public PropertyMap<ProductEntry, ProductEntryDto> toDto = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source.getProduct().getId(), destination.getProductId());
        }
    };

}
