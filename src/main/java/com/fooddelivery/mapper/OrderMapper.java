package com.fooddelivery.mapper;

import com.fooddelivery.dto.OrderDto;
import com.fooddelivery.model.Order;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper {

    public PropertyMap<OrderDto, Order> toModel = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source.getUserId(), destination.getUser().getId());
        }
    };

    public PropertyMap<Order, OrderDto> toDto = new PropertyMap<>() {
        @Override
        protected void configure() {
            map(source.getUser().getId(), destination.getUserId());
        }
    };

}
