package com.fooddelivery.data;

public interface DataInitializer {

    void initData();

}
