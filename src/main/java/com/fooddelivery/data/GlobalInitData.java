package com.fooddelivery.data;

import com.fooddelivery.model.*;
import com.fooddelivery.repository.OrderRepository;
import com.fooddelivery.repository.ProductRepository;
import com.fooddelivery.repository.SupplierRepository;
import com.fooddelivery.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class GlobalInitData implements DataInitializer {

    private final SupplierRepository supplierRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;

    public static List<User> users = List.of(
            User.builder()
                    .firstName("John")
                    .lastName("Silver")
                    .email("john@gmail.com")
                    .password("$2a$12$BAx1I78ZZkpipCyb8qejvuPWKz5AAVLfehrOF0jWY3v.COAnnHUfu")
                    .city("Lviv")
                    .build(),
            User.builder()
                    .firstName("Bill")
                    .lastName("Jonson")
                    .email("bill@gmail.com")
                    .password("$2a$12$BAx1I78ZZkpipCyb8qejvuPWKz5AAVLfehrOF0jWY3v.COAnnHUfu")
                    .city("Lviv")
                    .build(),
            User.builder()
                    .firstName("Tom")
                    .lastName("Smith")
                    .email("tom@gmail.com")
                    .password("$2a$12$BAx1I78ZZkpipCyb8qejvuPWKz5AAVLfehrOF0jWY3v.COAnnHUfu")
                    .city("Lviv")
                    .build());

    public static List<Product> products = List.of(
            //Pizza 0 - 3
            Product.builder()
                    .name("Pizza facilisis")
                    .productType(ProductType.PIZZA)
                    .price(10)
                    .dimensions(new Dimensions(30, 5, 30, 300))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c384252c.webp")
                    .build(),
            Product.builder()
                    .name("Pizza sed")
                    .productType(ProductType.PIZZA)
                    .price(20)
                    .dimensions(new Dimensions(30, 5, 30, 400))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c384252d.webp")
                    .build(),
            Product.builder()
                    .name("Pizza etiam")
                    .productType(ProductType.PIZZA)
                    .price(15.2)
                    .dimensions(new Dimensions(30, 5, 30, 450))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c384252e.webp")
                    .build(),
            Product.builder()
                    .name("Pizza morbi")
                    .productType(ProductType.PIZZA)
                    .price(6.7)
                    .dimensions(new Dimensions(30, 5, 30, 320))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c384252f.webp")
                    .build(),
            //Pasta 4 - 6
            Product.builder()
                    .name("Pasta porttitor ")
                    .productType(ProductType.PASTA)
                    .price(10)
                    .dimensions(new Dimensions(30, 5, 30, 320))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842530.webp")
                    .build(),
            Product.builder()
                    .name("Pasta libero")
                    .productType(ProductType.PASTA)
                    .price(12)
                    .dimensions(new Dimensions(30, 5, 30, 320))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842531.webp")
                    .build(),
            Product.builder()
                    .name("Pasta augue")
                    .productType(ProductType.PASTA)
                    .price(13)
                    .dimensions(new Dimensions(30, 5, 30, 320))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842532.jpeg")
                    .build(),
            //Burger 7 - 9
            Product.builder()
                    .name("Burger eget")
                    .productType(ProductType.BURGER)
                    .price(10)
                    .dimensions(new Dimensions(30, 5, 30, 200))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842533.webp")
                    .build(),
            Product.builder()
                    .name("Burger aliquam")
                    .productType(ProductType.BURGER)
                    .price(11)
                    .dimensions(new Dimensions(30, 5, 30, 220))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842534.jpeg")
                    .build(),
            Product.builder()
                    .name("Burger rhoncus")
                    .productType(ProductType.BURGER)
                    .price(12)
                    .dimensions(new Dimensions(30, 5, 30, 305))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb92e556c727754c3842535.webp")
                    .build(),
            //Salad 10 - 12
            Product.builder()
                    .name("Salad integer")
                    .productType(ProductType.SALAD)
                    .price(5)
                    .dimensions(new Dimensions(30, 5, 30, 100))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b025.webp")
                    .build(),
            Product.builder()
                    .name("Salad praesent")
                    .productType(ProductType.SALAD)
                    .price(7.3)
                    .dimensions(new Dimensions(30, 5, 30, 150))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b026.webp")
                    .build(),
            Product.builder()
                    .name("Salad proin")
                    .productType(ProductType.SALAD)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b027.webp")
                    .build(),
            //Bakery 13 - 15
            Product.builder()
                    .name("Baguette pellentesque")
                    .productType(ProductType.BREAD)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b028.jpeg")
                    .build(),
            Product.builder()
                    .name("Croissant vitae")
                    .productType(ProductType.CROISSANT)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b029.jpeg")
                    .build(),
            Product.builder()
                    .name("Croissant sapien")
                    .productType(ProductType.CROISSANT)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02a.jpeg")
                    .build(),
            //Soup 16 - 18
            Product.builder()
                    .name("Soup amet")
                    .productType(ProductType.SOUP)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02b.webp")
                    .build(),
            Product.builder()
                    .name("Soup lectus")
                    .productType(ProductType.SOUP)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02c.webp")
                    .build(),
            Product.builder()
                    .name("Soup sit")
                    .productType(ProductType.SOUP)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02d.webp")
                    .build(),
            //Deserts 19 - 21
            Product.builder()
                    .name("Desert risus")
                    .productType(ProductType.DESERT)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02e.webp")
                    .build(),
            Product.builder()
                    .name("Desert egestas")
                    .productType(ProductType.DESERT)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b02f.webp")
                    .build(),
            Product.builder()
                    .name("Desert quis")
                    .productType(ProductType.DESERT)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b030.webp")
                    .build(),
            //ROLL 22 - 25
            Product.builder()
                    .name("Roll donec")
                    .productType(ProductType.ROLL)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b031.webp")
                    .build(),
            Product.builder()
                    .name("Roll felis")
                    .productType(ProductType.ROLL)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b032.webp")
                    .build(),
            Product.builder()
                    .name("Roll quis")
                    .productType(ProductType.ROLL)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b033.webp")
                    .build(),
            Product.builder()
                    .name("Roll semper")
                    .productType(ProductType.ROLL)
                    .price(8)
                    .dimensions(new Dimensions(30, 5, 30, 185))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/product_5eb95176857e200d2a55b034.webp")
                    .build()
    );

    public static List<Supplier> suppliers = List.of(
            Supplier.builder()
                    .name("Amet risus")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.ITALIAN, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c3842546.jpeg")
                    .build(),
            Supplier.builder()
                    .name("Metus vulputate")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.ITALIAN, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c3842547.jpeg")
                    .build(),
            Supplier.builder()
                    .name("Habitasse platea")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.AMERICAN, SupplierTag.FAST_FOOD, SupplierTag.DESSERT, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c3842548.webp")
                    .build(),
            Supplier.builder()
                    .name("Ipsum suspendisse")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.BAKERY, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c3842549.jpeg")
                    .build(),
            Supplier.builder()
                    .name("Placerat in")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.ASIAN, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c384254a.webp")
                    .build(),
            Supplier.builder()
                    .name("Faucibus turpis")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.ASIAN, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c384254b.webp")
                    .build(),
            Supplier.builder()
                    .name("Scelerisque felis")
                    .address("Broadway, Newark")
                    .tags(Set.of(SupplierTag.DESSERT, SupplierTag.ALL))
                    .imageUrl("https://food-delivery-service-image-storage.s3.eu-central-1.amazonaws.com/supplier_5eb92e556c727754c384254c.webp")
                    .build()
    );

    List<Order> orders = List.of(
            Order.builder()
                    .creationDate(LocalDateTime.now())
                    .status(OrderStatus.DONE)
                    .total(25.2)
                    .build(),
            Order.builder()
                    .creationDate(LocalDateTime.now())
                    .status(OrderStatus.DONE)
                    .total(18)
                    .build()
    );

    @PostConstruct
    @Override
    public void initData() {
        userRepository.insert(users);
        users = userRepository.findAll();

        orderRepository.insert(orders);
        orders = orderRepository.findAll();

        productRepository.insert(products);
        products = productRepository.findAll();

        supplierRepository.insert(suppliers);
        suppliers = supplierRepository.findAll();

        //Italian
        products.get(0).setSupplier(suppliers.get(0));
        products.get(1).setSupplier(suppliers.get(0));
        products.get(4).setSupplier(suppliers.get(0));
        products.get(5).setSupplier(suppliers.get(0));
        suppliers.get(0).setProducts(
                List.of(
                        products.get(0),
                        products.get(1),
                        products.get(4),
                        products.get(5)
                ));
        products.get(2).setSupplier(suppliers.get(1));
        products.get(3).setSupplier(suppliers.get(1));
        products.get(6).setSupplier(suppliers.get(1));
        suppliers.get(1).setProducts(
                List.of(
                        products.get(2),
                        products.get(3),
                        products.get(6)
                ));

        //American
        products.get(7).setSupplier(suppliers.get(2));
        products.get(8).setSupplier(suppliers.get(2));
        products.get(9).setSupplier(suppliers.get(2));
        products.get(19).setSupplier(suppliers.get(2));
        products.get(10).setSupplier(suppliers.get(2));
        suppliers.get(2).setProducts(
                List.of(
                        products.get(7),
                        products.get(8),
                        products.get(9),
                        products.get(19),
                        products.get(10),
                        products.get(9)
                ));

        //Bakery
        products.get(13).setSupplier(suppliers.get(3));
        products.get(14).setSupplier(suppliers.get(3));
        products.get(15).setSupplier(suppliers.get(3));
        suppliers.get(3).setProducts(
                List.of(
                        products.get(13),
                        products.get(14),
                        products.get(15)
                ));

        //ASIAN
        products.get(16).setSupplier(suppliers.get(4));
        products.get(17).setSupplier(suppliers.get(4));
        products.get(11).setSupplier(suppliers.get(4));
        products.get(22).setSupplier(suppliers.get(4));
        products.get(23).setSupplier(suppliers.get(4));
        suppliers.get(4).setProducts(
                List.of(
                        products.get(16),
                        products.get(17),
                        products.get(11),
                        products.get(22),
                        products.get(23)
                ));
        products.get(18).setSupplier(suppliers.get(5));
        products.get(12).setSupplier(suppliers.get(5));
        products.get(24).setSupplier(suppliers.get(5));
        products.get(25).setSupplier(suppliers.get(5));
        suppliers.get(5).setProducts(
                List.of(
                        products.get(18),
                        products.get(15),
                        products.get(24),
                        products.get(25)
                ));

        //Desert
        products.get(20).setSupplier(suppliers.get(6));
        products.get(21).setSupplier(suppliers.get(6));
        suppliers.get(6).setProducts(
                List.of(
                        products.get(20),
                        products.get(21)
                ));

        supplierRepository.saveAll(suppliers);
        productRepository.saveAll(products);

        orders.get(0).setUser(users.get(0));
        orders.get(0).setProducts(List.of(
                new ProductEntry(suppliers.get(0).getProducts().get(0), 1),
                new ProductEntry(suppliers.get(1).getProducts().get(0), 1)
        ));
        users.get(0).setOrders(List.of(orders.get(0)));

        orders.get(1).setUser(users.get(1));
        orders.get(1).setProducts(List.of(
                new ProductEntry(suppliers.get(2).getProducts().get(0), 1),
                new ProductEntry(suppliers.get(3).getProducts().get(0), 1)
        ));
        users.get(1).setOrders(List.of(orders.get(1)));

        orderRepository.saveAll(orders);
        userRepository.saveAll(users);
    }

}