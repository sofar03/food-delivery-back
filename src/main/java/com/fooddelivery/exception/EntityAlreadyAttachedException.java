package com.fooddelivery.exception;

public class EntityAlreadyAttachedException extends RuntimeException {

    public EntityAlreadyAttachedException(String message) {
        super(message);
    }

}
